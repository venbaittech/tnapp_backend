<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/Custom_Model.php';
class Template_model extends Custom_Model {

		public function __construct() 
		{
        	parent::__construct();
        }

        public function getAllTemplates()
        {
        		$templates = $this->db->SELECT('template_id,template_name,template_desc')->FROM(INSPECTION_TEMPLATES)->GET()->result_array();
        		if(count($templates))
        		{
        			foreach ($templates as $key => $template) 
        			{
        				$template_questions = $this->db->SELECT('ques_ans_json,section')->FROM(INSPECTION_QUESTIONS)->WHERE('fk_template_id',$template['template_id'])->GET()->result_array();
        				$templates[$key]['questions'] = $template_questions;
        			}
        		}

        		return $templates;
        }


        public function getAllLeaningOutcomeQuestions()
        {
                        $learning_outcome = $this->db->SELECT('learning_outcome_id, grade, subject, unit, learning_outcome,')->FROM(LEARNING_OUTCOME_QUESTIONS)->GET()->result_array();
                        if(count($learning_outcome))
                        {
                                foreach ($learning_outcome as $key => $outcome) 
                                {
                                        $learning_outcome_questions = $this->db->SELECT('ques_ans_json,section')->FROM(INSPECTION_QUESTIONS)->WHERE('fk_learning_outcome_id',$outcome['learning_outcome_id'])->GET()->result_array();
                                        $learning_outcome[$key]['questions'] = $learning_outcome_questions;
                                }
                        }

                        return $learning_outcome;
        }
		
		function saveObservation($records,$created_by_user_name)
		{
			$data = array('json' => $records,'createdby' => $created_by_user_name['emis_username']);
			$id = $this->db->insert(SCHOOL_OBSERVATIONS, $data);
			return $id;
		}
        


}