<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

/*
 * Changes:
 * 1. This project contains .htaccess file for windows machine.
 *    Please update as per your requirements.
 *    Samples (Win/Linux): http://stackoverflow.com/questions/28525870/removing-index-php-from-url-in-codeigniter-on-mandriva
 *
 * 2. Change 'encryption_key' in application\config\config.php
 *    Link for encryption_key: http://jeffreybarke.net/tools/codeigniter-encryption-key-generator/
 * 
 * 3. Change 'jwt_key' in application\config\jwt.php
 *
 */

class Template extends REST_Controller
{
    /**
     * URL: http://localhost/CodeIgniter-JWT-Sample/auth/token
     * Method: GET
     */

     function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Template_model');
       
       
    }



    public function getTemplates_get()
    {
        $result_data = $this->Template_model->getAllTemplates();
        if(count($result_data))
        {
                $data['dataStatus'] = true;
                $data['status'] = REST_Controller::HTTP_OK;
                $data['records'] = $result_data;
                $this->response($data,REST_Controller::HTTP_OK);
        }   
        else
        {
                $data['dataStatus'] = false;
                $data['status'] = REST_Controller::HTTP_NOT_FOUND;
                $data['msg'] = 'Templates Not Found!';
                $this->response($data,REST_Controller::HTTP_OK);
        }
    }

    public function saveObservation_post()
    {
        //print_r($this->db->get(SCHOOL_OBSERVATIONS)->result_array());die();
        $records = $this->post('records');
       $created_by_user_name = $records[0];
       unset($records[0]);
       $id = $this->Template_model->saveObservation(json_encode($records),$created_by_user_name);
       if($id)
       {
                $data['dataStatus'] = true;
                $data['status'] = REST_Controller::HTTP_OK;
                $this->response($data,REST_Controller::HTTP_OK);
       }
       else
       {
            $data['dataStatus'] = false;
            $data['status'] = REST_Controller::HTTP_NOT_FOUND;
            $data['msg'] = 'Try later';
            $this->response($data,REST_Controller::HTTP_OK);
       }
    }

    public function getLeaningOutcomeQuestions_get()
    {
        $result_data = $this->Template_model->getAllLeaningOutcomeQuestions();
        if(count($result_data))
        {
                $data['dataStatus'] = true;
                $data['status'] = REST_Controller::HTTP_OK;
                $data['records'] = $result_data;
                $this->response($data,REST_Controller::HTTP_OK);
        }   
        else
        {
                $data['dataStatus'] = false;
                $data['status'] = REST_Controller::HTTP_NOT_FOUND;
                $data['msg'] = 'Templates Not Found!';
                $this->response($data,REST_Controller::HTTP_OK);
        }
    }
    

}